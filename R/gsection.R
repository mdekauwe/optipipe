###########################################################################

# Golden section search for N_f and rho_r = f(N_f) that maximize dH subject
# to dHcb = dHnb

# gsection is from Jones, Maillardet and Robinson (2009) (p. 206 - 207)

# ftn is dH_calc( )
gsection <- function(ftn, x.l, x.r, x.m, tol = 1e-6){
  
  # golden ratio plus one
  gr1 <- 1 + (1 + sqrt(5))/2
  
  # successivley refine x.l, x.r, and x.m
  f.l <- ftn(x.l)
  f.r <- ftn(x.r)
  f.m <- ftn(x.m)
  while ((x.r - x.l) > tol) {
    if ((x.r - x.m) > (x.m - x.l)){
      y <- x.m + (x.r - x.m)/gr1
      f.y <- ftn(y)
      if (f.y >= f.m){
        x.l <- x.m
        f.l <- f.m
        x.m <- y
        f.m <- f.y
      } else {
        x.r <- y
        f.r <- f.y
      }
    } else {
      y <- x.m - (x.m - x.l)/gr1
      f.y <- ftn(y)
      if (f.y >= f.m) {
        x.r <- x.m
        f.r <- f.m
        x.m <- y
        f.m <- f.y
      } else {
        x.l <- y
        f.l <- f.y
      }
    }
  }
  return( x.m )
}

###########################################################################
